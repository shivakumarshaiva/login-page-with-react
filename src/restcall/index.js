import axios from "axios";

export const apiCallService = (requestConfig, isMultipart = false, isTokenNeed = true) => {
  
  const token = localStorage.getItem('token');

  if (isMultipart) {
     requestConfig.headers = {
       'content-type': 'multipart/form-data',
     }
  } else if (isTokenNeed) {
    requestConfig.headers = {
      'Authorization': `Bearer ${token}`,
    }
  } 
  
  console.log('apiCallService :', requestConfig);
  
  return axios(requestConfig);
}