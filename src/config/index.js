function getApiUrl() {
  if (window.location.hostname === "localhost") {
    return 'https://service.intentx.com/';
  }
}

const configs = {
  BASE_URL: getApiUrl(),
}

export default configs;