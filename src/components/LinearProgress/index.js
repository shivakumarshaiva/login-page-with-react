import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
});

function LinearIndeterminate(props) {
  const { classes } = props;
  return (
    <div className={classes.root}>
      <LinearProgress style={{ backgroundColor: '#eee', height:'4px'}}/>
    </div>
  );
}

export default withStyles(styles, { withTheme: true })(LinearIndeterminate);