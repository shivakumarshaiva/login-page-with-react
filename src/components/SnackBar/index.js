import React from 'react';
import { withRouter} from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import {Snackbar, SnackbarContent, IconButton} from '@material-ui/core';

const styles = theme => ({
  close: {
    width: theme.spacing.unit * 4,
    height: theme.spacing.unit * 4,
  },
  error: {
    maxWidth: 'none',
    backgroundColor: '#ff5252'
  },
  info : {
    maxWidth: 'none',
    backgroundColor: theme.palette.primary.main,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    fontFamily: theme.typography.fontFamily,
    fontWeight: 400,
    fontSize: 'initial'
  },
  icon: {
    marginBottom: 5
  }
});

class ProsessSnackbar extends React.Component {
  
  render() {

    const { classes , snakeOpen, message, isError, setSnakeOpen, handleSnakeOpen } = this.props;

    return (
      <Snackbar
        open={snakeOpen}
        onClose={setSnakeOpen ? () => {setSnakeOpen(false)} : handleSnakeOpen}
        autoHideDuration={6000}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
      >
        <SnackbarContent
          className={isError ? classes.error : classes.info}
          aria-describedby="client-snackbar"
          variant="error"
          message={
            <span id="client-snackbar" className={classes.message}>
              {message}
            </span>
          }
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={setSnakeOpen ? () => {setSnakeOpen(false)} : handleSnakeOpen}
            >
              <CloseIcon className={classes.icon} />
            </IconButton>,
          ]}
        />
      </Snackbar>
    );
  }
}

export default (withStyles(styles, { withTheme: true })(withRouter(ProsessSnackbar)));