import React, {useState} from 'react';
import { withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import Avatar from '@material-ui/core/Avatar';
import FormHelperText from '@material-ui/core/FormHelperText';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import SnackBar from '../../../components/SnackBar';
import LinearProgress from '../../../components/LinearProgress';

const styles = theme => ({
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundRepeat: 'no-repeat',
    background: theme.palette.primary.light,
    backgroundSize: 'cover',
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
    margin: '0 auto',
  },
  card: {
    minWidth: 300,
    margin: 'auto auto',
  },
  form: {
    padding: '1em 1em 1em 1em',
  },
  button: {
    marginTop: '20px',
    color: '#fff',
    width: '100%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  font: {
    fontSize: '.9rem',
    fontWeight: 300,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    width: '100%'
  },
});


const ForgotPassword = (props) => {
  const { history, classes, theme } = props;

  const [userName, setUserName] = useState('');
  const [isUserNameMissing, setIsUserNameMissing] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [snakeOpen, setSnakeOpen] = useState(false);
  const [snackMessage, setSnackMessage] = useState('');
  const [isError, setIsError] = useState(false);

  const submitHandler = (event) => {
   alert("submit");
  }

  return (
    <div className={classes.main}>
      <SnackBar 
        snakeOpen={snakeOpen}
        message={snackMessage}
        isError = {isError}
        setSnakeOpen = {setSnakeOpen}
      />
      <Card className={classes.card} >
        <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
          {isLoading && <LinearProgress />}
        </div>
        <CardContent style={{ paddingBottom: '0px' }}>
          <Avatar aria-label="Recipe" className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography variant="title" className={classes.font}>
            Enter your valid e-mail to receive password!
          </Typography>      
        </CardContent>
        <form className={classes.form} noValidate autoComplete="off">
          <TextField
            style={{ margin: '0 auto' }}
            id="name"
            label="User Name"
            value={userName}
            onChange={(event) => {setUserName(event.target.value); setIsUserNameMissing(event.target.value ? false : true)}}
            margin="normal"
            fullWidth
            error={isUserNameMissing}
            helperText={isUserNameMissing ? 'Please Enter UserName / E-mail' : ''}
          /> <br />
          <Button
            className={classes.button}
            type="submit"
            id="createButton"
            disabled={isLoading}
            name="Submit"
            onClick={submitHandler}
          >
            SEND YOUR PASSWORD
          </Button>
        </form>
      </Card>
    </div>
  );
}

ForgotPassword.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default (withStyles(styles, { withTheme: true })(withRouter(ForgotPassword)));