import React, {useState, useEffect}from 'react';
import {Link, withRouter} from 'react-router-dom';
import {withStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import Avatar from '@material-ui/core/Avatar';
import FormHelperText from '@material-ui/core/FormHelperText';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Button from '@material-ui/core/Button';
import LinearProgress from '../../../components/LinearProgress';
import SnackBar from '../../../components/SnackBar';
import { LOGIN_URL } from '../../API/EndPoints';
import { apiCallService } from '../../../restcall';

const styles = theme => ({
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    minHeight: '100vh',
    alignItems: 'center',
    justifyContent: 'flex-start',
    background: theme.palette.primary.light,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  avatar: {
    backgroundColor: theme.palette.primary.main,
  },
  card: {
    minWidth: 300,
    margin: 'auto auto',
  },
  form: {
    padding: '1em 1em 1em 1em',
  },
  button: {
    marginTop: '20px',
    height: '46px',
    color: '#fff',
    width: '100%',
    borderRadius: '5px',
    backgroundColor: theme.palette.primary.main,
    '&:hover': {
      backgroundColor: theme.palette.primary.dark,
    },
  },
  logo: {
    flexGrow: 1,
    alt: 'No logo found',
    margin: '3px 30px',
    width: '250px',
    '@media (max-width: 500px)': {
      width: '250px',
    },
  },
  font: {
    fontSize: '.9rem',
    fontWeight: 300,
    color: theme.palette.primary.main,
    fontFamily: theme.typography.fontFamily,
    flexGrow: 1,
    width: '100%'
  },
});

const Login = (props) => {
  const { classes, theme, history} = props;

  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(false);
  const [isUserNameEmpty, setIsUserNameEmpty] = useState(false);
  const [isPasswordEmpty, setIsPasswordEmpty] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [snakeOpen, setSnakeOpen] = useState(false);
  const [snackMessage, setSnackMessage] = useState(false);
  
  const submitHandler = (event) => {
    event.preventDefault();
    setIsLoading(true);

    let formData = new FormData();
    formData.append('j_username', userName);
    formData.append('j_password', password);
   
    apiCallService({
        method: 'post',
        url: LOGIN_URL,
        data: formData,
      }, false, false)
      .then(function (res) {
      console.log('res :', res);
      window.localStorage.setItem('token', res.data.token);
      window.localStorage.setItem('clientId', res.data.clientId);
      setTimeout(() => setIsLoading(false), history.push('/dashboard'), 1000);
    }).catch(function (error) {
      console.log('error while logging in...', error);
      if (error && error.toString().includes("code 401")) {
        setSnackMessage('Invalid uresname or password, please check and login!!')
      } else {
        setSnackMessage('Something went wrong while login..., please contact system admin!!')
      }
      setIsLoading(false);
      setSnakeOpen(true);
    })
  }

  return (
    <div className={classes.main}>
      <SnackBar 
        snakeOpen={snakeOpen}
        message={snackMessage}
        isError = {true}
        setSnakeOpen = {setSnakeOpen}
      />
      <Card className={classes.card} >
        <div style={{ backgroundColor: theme.palette.primary.main, height: '4px' }}>
          {isLoading && <LinearProgress />}
        </div>
        <CardContent style={{ paddingBottom: '0px' }}>
          <div className={classes.logo}>
            <img src={'/intentx.png'} alt="pic" className={classes.logo} />
          </div>
        </CardContent>
        <form className={classes.form} onSubmit={submitHandler} autoComplete="off">              
          <TextField
            style={{margin:'0 auto'}}
            id="name"
            label="User Name"
            value={userName === '' ? '' : userName} 
            onChange={(event) => {setUserName(event.target.value); setIsUserNameEmpty(event.target.value ? false : true)}}
            margin="normal"
            fullWidth
            error={isUserNameEmpty}
            helperText={isUserNameEmpty ? 'Please Enter UserName / E-mail' : ''}
          /> <br/>
          <FormControl style={{width:'100%'}}>
            <InputLabel htmlFor="adornment-password">Password</InputLabel>
              <Input
                id="adornment-password"
                type={showPassword ? 'text' : 'password'}
                value={password === '' ?  '' : password}
                onChange={(event) => {setPassword(event.target.value); setIsPasswordEmpty(event.target.value ? false : true)}}
                fullWidth
                error={isPasswordEmpty}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Toggle password visibility"
                      onClick={() => setShowPassword(!showPassword)}
                      onMouseDown={(event) => event.preventDefault()}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff /> }
                    </IconButton>
                  </InputAdornment>
                }
              />
            {
              isPasswordEmpty &&
              <FormHelperText className={classes.red} id="name-error-text">
                Please Enter password
              </FormHelperText>
            }
            </FormControl>
    
          <Button
            key={1}
            className={classes.button}
            type="submit"
            disabled={isLoading}
            onClick={submitHandler}
            >
            <Avatar aria-label="Recipe" className={classes.avatar}>
              <LockIcon />
            </Avatar>
            SIGNIN
          </Button>
        </form>
        {/* <div style={{marginBottom:'14px', textAlign: 'center'}}>
          <Link to="forgot-password" className={classes.font}>
            Forgot Password
          </Link>
        </div> */}
      </Card>
    </div>
  );
}

export default (withStyles(styles, { withTheme: true })(withRouter(Login)));
