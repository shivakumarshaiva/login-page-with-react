import React from 'react';
import {Helmet} from 'react-helmet';
import styled from 'styled-components';
import {Route, Switch} from 'react-router-dom';
import {MuiThemeProvider} from '@material-ui/core/styles';
import theme from '../../theme';

//import ower own components
import DashBoard from '../DashBoard';
import LoginPage from '../Auth/Login';
import ForgotPassword from '../Auth/ForgotPassword'

const AppWrapper = styled.div `
  min-height: 100%;
`;


const Router = () => {

  return(
    < MuiThemeProvider theme={theme}>
      <AppWrapper>
        <Helmet titleTemplate="Admin" defaultTitle="Intentx Admin Tool">
          <meta name="description" content="Intentx Admin Tool" />
        </Helmet>
        <Switch>
          <Route path="/dashboard" component={DashBoard} exact />
          <Route path="/login" component={LoginPage} exact />
          <Route path="/forgot-password" component={ForgotPassword} exact />
        </Switch>
      </AppWrapper>
    </MuiThemeProvider>

  )
}

export default Router;