import { createMuiTheme } from '@material-ui/core/styles';

export default createMuiTheme({
  palette: {
    primary: {
      light: '#4b9fea',
      main: '#1e88e5',
      dark: '#155fa0',
    },
    secondary: {
      light: '#4b9fea',
      main: '#1e88e5',
      dark: '#155fa0',
    },
  },
  typography: {
    fontFamily: 'Roboto, sans-serif'
  },
});